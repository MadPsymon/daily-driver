import configparser
import random
import spotipy
import logging
from pyparsing import AtStringStart
from spotipy.oauth2 import SpotifyOAuth
from pprint import pprint

config = configparser.ConfigParser()
config.read('config.ini')

log_level_info = {'DEBUG': logging.DEBUG,
                  'INFO': logging.INFO,
                  'WARNING': logging.WARNING,
                  'ERROR': logging.ERROR,
                 }
log_level_config = config.get('logging', 'log_level')
log_level = log_level_info.get(log_level_config, logging.ERROR)
logging.basicConfig(level=log_level,
                    format="{asctime} - {levelname} - {message}",
                    style="{",
                    datefmt="%Y-%m-%d %H:%M",
                    )

scope = ["playlist-modify-public", "user-follow-read", "user-library-read", "user-read-playback-position"]

client_id=config.get('spotify', 'client_id')
client_secret=config.get('spotify', 'client_secret')
redirect_url=config.get('spotify', 'redirect_url')

FETCH_ARTIST_REPETITIONS = config.getint('daily_driver', 'fetch_artist_repetitions')
LEN_SONG_BLOCK = config.getint('daily_driver', 'len_song_block')
NUM_SONG_BLOCKS = config.getint('daily_driver', 'num_song_blocks')

sp = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=client_id,
                                               client_secret=client_secret,
                                               redirect_uri=redirect_url,
                                               scope=scope,
                                               open_browser=False))

# Liste mit Liedern erstellen
results_artist_top_tracks = {'artists': {'items': []}}
track_list = []

logging.info("Liste mit Top Tracks wird erstellt")
for i in range(FETCH_ARTIST_REPETITIONS):
    followed_artists = sp.current_user_followed_artists(limit=50, after = i * 50)

    for artist in followed_artists['artists']['items']:
        results_artist_top_tracks['artists']['items'].append(artist)
for idx, item in enumerate(results_artist_top_tracks['artists']['items']):
    top_tracks = sp.artist_top_tracks(artist_id=item['id'])
    for idx, track in enumerate(top_tracks['tracks']):
        track_list.append(track['id'])

# Liste mit Podcast Episoden erstellen
episode_list = []
results_show_episodes = sp.current_user_saved_shows()
logging.info("Liste mit Podcast-Episoden wird abgerufen")
for idx, item  in enumerate(results_show_episodes['items']):
    id = item['show']['id']
    episodes = sp.show_episodes(show_id=id,limit=1)
    last_episode = episodes['items'][0]
    if last_episode['resume_point']['fully_played'] is False:
        episode_list.append(last_episode['uri'])

# Trackliste mischen
random.shuffle(track_list)

# Alte Spotify-Playlist löschen
user = sp.current_user()
sp_user_playlists = sp.user_playlists(user=user['id'],limit=50)
sp_playlist_old = [item for item in sp_user_playlists['items'] if item['name'] == 'Daily-Driver']
logging.info("Alte Playlist wird gesucht")
if len(sp_playlist_old) != 0:
    logging.info("Alte Playlist gefunden: {}".format(sp_playlist_old[0]['id']))
    sp.user_playlist_unfollow(user=user['id'],
                              playlist_id=sp_playlist_old[0]['id'])
logging.info("Playlist gelöscht")
# Neue Spotify-Playlist erstellen

logging.info("Neue Playlist erstellen")
sp_playlist = sp.user_playlist_create(user=user['id'], 
                                      name="Daily-Driver",
                                      public=True,
                                      collaborative=False,
                                      description='Automatisch generierte Daily-Driver Playlist')

# Spotify-Playlist befüllen
logging.info("Playlist befüllen")
for i in range(NUM_SONG_BLOCKS):
    if len(episode_list) != 0:
        sp.playlist_add_items(playlist_id=sp_playlist['id'],
                               items=[episode_list.pop(0)],
                               position=None)
    for i in range(0,LEN_SONG_BLOCK):
        sp.playlist_add_items(playlist_id=sp_playlist['id'],
                              items=[track_list.pop()],
                              position=None)
logging.info("Playlist befüllt")

